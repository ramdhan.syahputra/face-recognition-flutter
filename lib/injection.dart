import 'package:fr_new/services/hive_service.dart';
import 'package:fr_new/services/ml_service.dart';
import 'package:get_it/get_it.dart';

final di = GetIt.instance;

initialize() async {
  di.registerLazySingleton(() => MlService(di()));
  di.registerLazySingleton(() => HiveService());
}
