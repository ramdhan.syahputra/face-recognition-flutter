import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:hive/hive.dart';

class HiveService {
  static String userKey = "user";
  static String boxName = "faces";
  static String collectionName = "smart_office";

  late BoxCollection _collection;
  late CollectionBox _collectionBox;

  initialize() async {
    Directory directory = await getTemporaryDirectory();

    Hive.init(directory.path);

    _collection = await BoxCollection.open(
      collectionName,
      {boxName},
      path: directory.path,
    );

    _collectionBox = await _collection.openBox(boxName);
  }

  putUser(String key, List value) async => await _collectionBox.put(key, value);

  getUser(String key) async => await _collectionBox.get(key);

  Future<List<String>> getUserKeys() async => await _collectionBox.getAllKeys();

  Future<Map<String, dynamic>> getUsers() async =>
      await _collectionBox.getAllValues();

  removeUsers() => _collectionBox.clear();

  dispose() async => _collection.close();
}
