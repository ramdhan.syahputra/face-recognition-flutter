import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:fr_new/services/hive_service.dart';
import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';
import 'dart:developer' as dev;
import 'package:image/image.dart' as imglib;
import 'package:tflite_flutter/tflite_flutter.dart';

import '../utils/image_converter.dart';

class MlService {
  MlService(this._hiveService);
  final HiveService _hiveService;

  double threshold = Platform.isAndroid ? 0.3 : 1.0;

  Interpreter? interpreter;
  IsolateInterpreter? isolateInterpreter;
  List _predictedData = [];

  String get modelName => "assets/mobilefacenet.tflite";

  List get predictedData => _predictedData;

  Future initialize() async {
    try {
      var interpreterOptions = InterpreterOptions();
      interpreter = await Interpreter.fromAsset(
        modelName,
        options: interpreterOptions,
      );
      isolateInterpreter =
          await IsolateInterpreter.create(address: interpreter!.address);
    } catch (e) {
      dev.log(name: "ML SERVICE: ", 'Failed to load tflite model.');
      dev.log(name: "ML SERVICE: ", e.toString());
    }
  }

  Future<List> setCurrentPrediction(String path, Face? faceDetected) async {
    if (isolateInterpreter == null) throw Exception('Interpreter is null');
    List input = await _preProcessFile(path, faceDetected);

    input = input.reshape([1, 112, 112, 3]);
    List output = List.generate(1, (index) => List.filled(192, 0));

    await isolateInterpreter?.run(input, output);
    output = output.reshape([192]);

    return List.from(output);
  }

  void setCurrentPrediction2(CameraImage cameraImage, Face? face) async {
    if (face == null) throw Exception('Face is null');
    List input = _preProcess(cameraImage, face);

    // dev.log(name: "ML SERVICE: ", input.toString());

    input = input.reshape([1, 112, 112, 3]);
    List output = List.generate(1, (index) => List.filled(192, 0));

    await isolateInterpreter?.run(input, output);
    output = output.reshape([192]);

    _predictedData = List.from(output);
  }

  List _preProcess(CameraImage image, Face faceDetected) {
    imglib.Image croppedImage = _cropFace(image, faceDetected);
    imglib.Image img = imglib.copyResizeCropSquare(croppedImage, 112);

    Float32List imageAsList = imageToByteListFloat32(img);
    return imageAsList;
  }

  imglib.Image _cropFace(CameraImage image, Face faceDetected) {
    imglib.Image convertedImage = _convertCameraImage(image);
    double x = faceDetected.boundingBox.left - 10.0;
    double y = faceDetected.boundingBox.top - 10.0;
    double w = faceDetected.boundingBox.width + 10.0;
    double h = faceDetected.boundingBox.height + 10.0;

    return imglib.copyCrop(
      convertedImage,
      x.round(),
      y.round(),
      w.round(),
      h.round(),
    );
  }

  imglib.Image _convertCameraImage(CameraImage image) {
    var img = convertToImage(image);
    var img1 = imglib.copyRotate(img, -90);
    return img1;
  }

  Future<List> _preProcessFile(String path, Face? faceDetected) async {
    File file = File(path);
    final img = await file.readAsBytes();
    imglib.Image? imgFromfile = imglib.decodeImage(img);

    final imglib.Image croppedImage;

    if (faceDetected != null) {
      croppedImage = _cropFaceImage(imgFromfile!, faceDetected);
    } else {
      croppedImage = imglib.copyResizeCropSquare(imgFromfile!, 112);
    }

    Float32List imageAsList = imageToByteListFloat32(croppedImage);
    return imageAsList;
  }

  imglib.Image _cropFaceImage(imglib.Image image, Face faceDetected) {
    double x = faceDetected.boundingBox.left - 10.0;
    double y = faceDetected.boundingBox.top - 10.0;
    double w = faceDetected.boundingBox.width + 10.0;
    double h = faceDetected.boundingBox.height + 10.0;
    return imglib.copyCrop(image, x.round(), y.round(), w.round(), h.round());
  }

  Float32List imageToByteListFloat32(imglib.Image image) {
    var convertedBytes = Float32List(1 * 112 * 112 * 3);
    var buffer = Float32List.view(convertedBytes.buffer);
    int pixelIndex = 0;

    for (var i = 0; i < 112; i++) {
      for (var j = 0; j < 112; j++) {
        var pixel = image.getPixel(j, i);
        buffer[pixelIndex++] = (imglib.getRed(pixel) - 128) / 128;
        buffer[pixelIndex++] = (imglib.getGreen(pixel) - 128) / 128;
        buffer[pixelIndex++] = (imglib.getBlue(pixel) - 128) / 128;
      }
    }
    return convertedBytes.buffer.asFloat32List();
  }

  Future<(String?, double)?> searchResult(List predictedData) async {
    final users = await _hiveService.getUsers();
    double minDist = 999;
    double currDist = 0.0;
    String? predictedResult;

    dev.log(name: "SEARCH RESULT", users.keys.toList().toString());

    for (var user in users.entries) {
      dev.log(name: 'SEARCH RESULT: ', user.key);
      currDist = _euclideanDistance(predictedData, user.value);

      if (currDist == 0.0) return null;

      if (currDist <= threshold && currDist < minDist) {
        dev.log(name: 'SEARCH RESULT: ', '$currDist');
        minDist = currDist;
        final username = user.key;
        predictedResult = username;
      }
    }

    return (predictedResult, currDist);
  }

  double _euclideanDistance(List? e1, List? e2) {
    if (e1 == null || e2 == null) throw Exception("Null argument");

    double sum = 0;
    for (int i = 0; i < e1.length; i++) {
      sum += pow((e1[i] - e2[i]), 2);
    }

    return pow(sum, 0.5).toDouble();
  }

  // double _euclideanDistance(List? e1, List? e2) {
  //   if (e1 == null || e2 == null) throw Exception("Null argument");

  //   double sum = 0.0;
  //   for (int i = 0; i < e1.length; i++) {
  //     sum += pow((e1[i] - e2[i]), 2);
  //   }

  //   return sum;
  // }

  dispose() {
    interpreter?.close();
    isolateInterpreter?.close();
  }
}
