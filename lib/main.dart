import 'package:flutter/material.dart';
import 'package:fr_new/injection.dart';
import 'package:fr_new/pages/home_page.dart';
import 'package:fr_new/services/hive_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initialize();
  final hive = di.get<HiveService>();
  await hive.initialize();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Face recognition new',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.red),
        useMaterial3: true,
      ),
      home: const HomePage(),
    );
  }
}
