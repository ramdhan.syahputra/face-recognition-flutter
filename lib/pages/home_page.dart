import 'dart:developer';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fr_new/injection.dart';
import 'package:fr_new/services/hive_service.dart';
import 'package:fr_new/services/ml_service.dart';
import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';
// import 'package:restart_app/restart_app.dart';

import 'painter/face_detector_painter.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  static List<CameraDescription> _cameras = [];
  CameraController? _controller;
  InputImage? _inputImage;

  bool _canProcess = true; // to prevent uneccesary process for ML prediction
  bool _isBusy = false; // to prevent uneccesary process for Face detection
  CustomPaint? _customPaint;
  List<Face>? _faces;
  String? user;
  double? currentDistance;
  List? predictedData;

  late HiveService _hiveService;
  late MlService _mlService;
  late TextEditingController _txtFormFieldController;

  final FaceDetector _faceDetector = FaceDetector(
    options: FaceDetectorOptions(
      enableContours: true,
      enableClassification: true,
      enableLandmarks: true,
      enableTracking: true,
      performanceMode: FaceDetectorMode.fast,
    ),
  );

  @override
  void initState() {
    _hiveService = di.get<HiveService>();
    _mlService = di.get<MlService>();
    _txtFormFieldController = TextEditingController();
    _initialize();
    super.initState();
  }

  _initialize() async {
    await _mlService.initialize();

    if (_cameras.isEmpty) {
      _cameras = await availableCameras();
    }

    final camera = _cameras[1];

    _controller = CameraController(
      camera,
      ResolutionPreset.high,
      enableAudio: false,
      imageFormatGroup: Platform.isAndroid
          ? ImageFormatGroup.nv21
          : ImageFormatGroup.bgra8888,
    );

    await _controller?.initialize();

    setState(() {});

    if (_controller == null) return;
    if (!_controller!.value.isInitialized) return;

    _controller?.startImageStream(_processCameraImage);
  }

  @override
  void dispose() {
    super.dispose();
    if (_controller != null) _controller?.dispose();
  }

  void _processCameraImage(CameraImage image) async {
    _inputImage = _inputImageFromCameraImage(image);

    // Face detection process
    if (_inputImage == null) return;
    if (_isBusy) return;

    _isBusy = true;

    _faces = await _faceDetector.processImage(_inputImage!);

    if (_inputImage!.metadata?.size != null &&
        _inputImage!.metadata?.rotation != null) {
      final painter = FaceDetectorPainter(
        _faces!,
        _inputImage!.metadata!.size,
        _inputImage!.metadata!.rotation,
        CameraLensDirection.front,
      );
      _customPaint = CustomPaint(painter: painter);
    }

    _isBusy = false;

    if (mounted) {
      setState(() {});
    }

    // ML detection process
    if (!_canProcess) return;
    if (_faces == null || _faces!.isEmpty) {
      user = null;
      return;
    }
    _canProcess = false;

    try {
      final face = _faces!.isEmpty ? null : _faces?.first;

      _canProcess = false;
      _mlService.setCurrentPrediction2(image, face);

      predictedData = _mlService.predictedData;

      final ((user, currentDistance)!) =
          await _mlService.searchResult(predictedData!);

      this.user = user;
      this.currentDistance = currentDistance;

      log(name: "PREDICTED: ", predictedData.toString());
    } catch (e) {
      log(name: "ML SERVICE: ", e.toString());
      _canProcess = true;
    }

    _canProcess = true;
  }

  final _orientations = {
    DeviceOrientation.portraitUp: 0,
    DeviceOrientation.landscapeLeft: 90,
    DeviceOrientation.portraitDown: 180,
    DeviceOrientation.landscapeRight: 270,
  };

  InputImage? _inputImageFromCameraImage(CameraImage image) {
    if (_controller == null) return null;

    final camera = _cameras[1];
    final sensorOrientation = camera.sensorOrientation;

    InputImageRotation? rotation;
    if (Platform.isIOS) {
      rotation = InputImageRotationValue.fromRawValue(sensorOrientation);
    } else if (Platform.isAndroid) {
      var rotationCompensation =
          _orientations[_controller!.value.deviceOrientation];

      if (rotationCompensation == null) return null;

      if (camera.lensDirection == CameraLensDirection.front) {
        rotationCompensation = (sensorOrientation + rotationCompensation) % 360;
      } else {
        rotationCompensation =
            (sensorOrientation - rotationCompensation + 360) % 360;
      }
      rotation = InputImageRotationValue.fromRawValue(rotationCompensation);
    }
    if (rotation == null) return null;

    final format = InputImageFormatValue.fromRawValue(image.format.raw);

    if (format == null ||
        (Platform.isAndroid && format != InputImageFormat.nv21) ||
        (Platform.isIOS && format != InputImageFormat.bgra8888)) return null;

    if (image.planes.length != 1) return null;
    final plane = image.planes.first;

    return InputImage.fromBytes(
      bytes: plane.bytes,
      metadata: InputImageMetadata(
        size: Size(image.width.toDouble(), image.height.toDouble()),
        rotation: rotation,
        format: format,
        bytesPerRow: plane.bytesPerRow,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: const Text("Fr new version"),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              currentDistance == null
                  ? "null"
                  : currentDistance!.toStringAsFixed(2),
              style: const TextStyle(color: Colors.green),
            ),
          ),
          IconButton(
            onPressed: () => _hiveService.removeUsers(),
            icon: const Icon(Icons.delete_forever),
          ),
        ],
      ),
      body: _controller == null ? _buildLoading() : _buildContent(),
      floatingActionButton: FloatingActionButton.large(
        onPressed: () {
          _controller?.stopImageStream();
          if (predictedData == null) {
            _showWarningBottomsheet(context).then((value) {
              _controller?.startImageStream(_processCameraImage);
            });
            return;
          }
          _showFormBottomsheet(context).then((value) {
            _controller?.startImageStream(_processCameraImage);
          });
        },
        child: const Icon(Icons.add),
      ),
    );
  }

  Future<dynamic> _showFormBottomsheet(BuildContext context) {
    return showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (context) {
        return Wrap(
          children: [
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom,
                top: 12,
                right: 12,
                left: 12,
              ),
              child: Column(
                children: [
                  const Text("Input your name"),
                  const SizedBox(height: 8),
                  TextFormField(controller: _txtFormFieldController),
                  const SizedBox(height: 12),
                  ElevatedButton.icon(
                    onPressed: () {
                      _hiveService.putUser(
                        _txtFormFieldController.value.text,
                        predictedData!,
                      );
                      _txtFormFieldController.clear();
                      Navigator.pop(context);
                    },
                    icon: const Icon(Icons.add),
                    label: const Text("Save"),
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  Future<dynamic> _showWarningBottomsheet(BuildContext context) {
    return showModalBottomSheet(
      context: context,
      builder: (context) {
        return Container(
          padding: const EdgeInsets.all(12),
          alignment: Alignment.center,
          child: const Text("Please try again.."),
        );
      },
    );
  }

  Widget _buildContent() {
    return Stack(
      children: [
        SizedBox.expand(
          child: CameraPreview(
            _controller!,
            child: _customPaint,
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.only(top: 12, right: 12, bottom: 24),
            child: Container(
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                color: Colors.pink,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(12),
              ),
              child: Text(
                user ?? "unknown",
                maxLines: 1,
                style: const TextStyle(color: Colors.white, fontSize: 32),
              ),
            ),
          ),
        ),
        // Align(
        //   alignment: Alignment.bottomCenter,
        //   child: Padding(
        //     padding: const EdgeInsets.only(bottom: 12),
        //     child: ElevatedButton.icon(
        //       onPressed: () async => await Restart.restartApp(),
        //       icon: const Icon(Icons.refresh_outlined),
        //       label: const Text("Restart"),
        //     ),
        //   ),
        // )
      ],
    );
  }

  Center _buildLoading() => const Center(child: Text("Wait a moment."));
}
